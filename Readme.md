# Description

  It's a demonstration idea is from friends in IoT area to get more lightweight application than in JBoss or Wildfly.
  
  This small project is based on spring boot framework and implements simple REST API in car catalog domain.
  The embeded Mongodb (https://github.com/flapdoodle-oss/de.flapdoodle.embed.mongo) is used as database of the application. Can easily change to MongoDB

# Guide

  This guide walks you through the process of building, running and testing the simple car catalog RESTful web service. 

   NOTE: 
   1) Following step description are based on Ubuntu Linux 64bit, the other Linux environment should be similar. 
   2) In the following steps of guidlines, all commands need to run in the Linux terminal are started with shell prompt "#", user of this guidline should type/copy commands without "#".

## Prerequisites

1) Internet connection that use to download softwares and libraries needed the project;
2) Install JDK1.8 or later (http://www.oracle.com/technetwork/java/javase/downloads/index.html)
3) Set JAVA_HOME and PATH environment variables (https://stackoverflow.com/questions/9612941/how-to-set-java-environment-path-in-ubuntu)
4) Check whether the Java settings works, run following command in the terminal (shell): 
	<pre><code>
	# java -version
    </code></pre>
	
	The output of above command should be similar to below example:
   <pre><code>
	java version "1.8.0_152"
	Java(TM) SE Runtime Environment (build 1.8.0_152-b16)
	Java HotSpot(TM) 64-Bit Server VM (build 25.152-b16, mixed mode)
    </code></pre>
5) Download (git clone) the project in one directory you like, below use "~/projects" as parent folder example, like:
   <pre><code>
   # cd ~/projects/
   # git clone <git_clone_url>/carcat
   </code></pre>

## How to build

1) At the terminal, change directory to "carcat"
   <pre><code>
   # cd carcatalog
   </code></pre>
2) Run the self-contained Maven wrapper command to build and package (On Linux)
   <pre><code>
   # ./mvnw clean package
   </code></pre>
   NOTE: On Windows should run `mvnw clean package`
   Within this step, the project unit tests are executed automatically, if unit tests run without failure, an executable JAR file "carcat-0.0.1-SNAPSHOT.jar" is generated in "target" sub-directory of current directory.

## How to run
   Run the executable JAR file "carcat-0.0.1-SNAPSHOT.jar" with following command:
   <pre><code>
   # java -jar target/carcat-0.0.1-SNAPSHOT.jar
   </code></pre>
   If above command is run successfully, you can see similar output:
<pre><code>
2017-12-03 19:37:44 - Starting beans in phase 2147483647
2017-12-03 19:37:44 - Context refreshed
2017-12-03 19:37:44 - Found 1 custom documentation plugin(s)
2017-12-03 19:37:44 - Scanning for api listing references
2017-12-03 19:37:45 - Tomcat started on port(s): 9292 (http)
2017-12-03 19:37:45 - Started CarCatApplication in 6.958 seconds (JVM running for 7.418)
2017-12-03 19:38:10 - Initializing Spring FrameworkServlet 'dispatcherServlet'
</code></pre>
   
   After application is up and embeded Tomcat is started up and its binding address and HTTP port are localhost:9292. You can simply check whether it works:
   <pre><code>
   # curl http://localhost:9292/carcat
   </code></pre>

## How to test the REST API web service
   For details of API usage, you can directly refer to embedded API documents, http://localhost:9292/swagger-ui.html#/Car_Catalog_API_document. In the document, you can check the message format, description of parameters and also you can try to use the API within the page by clicking the "Tyr it out" button.

   Below exampls are copied from the page.

1) Add one car entity to catalog
<pre><code>
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "number":"ABC123","brand":"BMW","manufactureDate": "2017-12-03T21:47:05.421Z","model": "X1","owner": "Alice"}' 'http://localhost:9292/carcat/add'
</code></pre>
2) Show one car entity in the catalog
<pre><code>
curl -X GET --header 'Accept: application/json' http://localhost:9292/carcat/ABC123
</code></pre>
3) Update the car entity properties
<pre><code>
curl -X PUT --header 'Content-Type: application/json' -d '{ "number":"ABC123","brand":"Volkswagen","manufactureDate": "2017-12-03T21:47:05.421Z","model": "POLO","owner": "Bob"}' 'http://localhost:9292/carcat/edit/ABC123'
</code></pre>
4) Query the car list by owner
<pre><code>
curl -X GET --header 'Accept: application/json' http://localhost:9292/carcat/searchByOwner?owner=Bob
</code></pre>
5) Query the car list by brand
<pre><code>
curl -X GET --header 'Accept: application/json' http://localhost:9292/carcat/searchByBrand?brand=Volkswagen
</code></pre>
6) Delete car entity from catalog database
<pre><code>
curl -X DELETE http://localhost:9292/carcat/delete/ABC123
</code></pre>

## (Optional) How to customize configurations
   Depends on your environment, the application's configuration can be customized if need.

1) Customize HTTP binding address and port, run java command (see step "2. how to run") with --server.address and --server.port, like:
   <pre><code>
   # java -jar target/carcat-0.0.1-SNAPSHOT.jar --server.address=myhostname --server.port=7070
   </code></pre>
   Or you can specify "server.address" and "server.port" in the application.properties file in "src/main/resources/application.properties" before build the package.

2) Logging settings and log file
   Similar to HTTP binding address and port, logging configuration can also be set via application properties.
   See the link for details: https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-logging.html

3) The embed mongodb directory can be configured with "spring.mongodb.embedded.storage.databaseDir" property in running command or application.properties file. Like:
   <pre><code>
   # java -jar target/carcat-0.0.1-SNAPSHOT.jar --spring.mongodb.embedded.storage.databaseDir=/tmp/mydata
   </code></pre>
   By default, the embedded Mongo database store data in system temporary directory (e.g., /tmp on Linux).

# Improvements

   1) In production environment, the security of the web service should be considered and this project has NOT implemented yet
   2) Existing unit tests has not covered all use cases
   3) Request parameters validation are not completely implemented
   n) etc.


