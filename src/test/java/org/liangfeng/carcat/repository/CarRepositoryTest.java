package org.liangfeng.carcat.repository;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.liangfeng.carcat.domain.Car;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CarRepositoryTest {

    @Autowired
    private CarRepository carRepository;

    @After
    public void cleanup(){
        carRepository.deleteAll();
    }
    @Test
    public void addCar() {
        Car car = new Car("Abc123", "Alice", "BMW", "X1", new Date());
        Car carCreated = carRepository.save(car);
        assertEquals(car.getNumber(), carCreated.getNumber());
    }
    @Test
    public void findCarByNumber() {
        Car car = new Car("FFF123", "Bob", "BMW", "X1", new Date());
        carRepository.save(car);
        Car carFound = carRepository.findByNumberIgnoreCase("fff123");
        assertEquals("Bob", carFound.getOwner());
        assertEquals("BMW", carFound.getBrand());
        assertEquals("X1", carFound.getModel());
    }
    @Test
    public void findCarByOwner() {
        Car car = new Car("Abc123", "Alice", "Volkswagen", "Polo", new Date());
        carRepository.save(car);
        Car car2 = new Car("Abc124", "Alice", "BMW", "X5", new Date());
        carRepository.save(car2);

        List<Car> carList = carRepository.findByOwnerIgnoreCase("aLice");

        assertTrue(carList.stream().filter(o -> o.getNumber().equals("Abc123")).findFirst().isPresent());
        assertTrue(carList.stream().filter(o -> o.getNumber().equals("Abc124")).findFirst().isPresent());

        assertTrue(carList.stream().filter(o -> o.getBrand().equals("Volkswagen")).findFirst().isPresent());
        assertTrue(carList.stream().filter(o -> o.getBrand().equals("BMW")).findFirst().isPresent());

        assertTrue(carList.stream().filter(o -> o.getModel().equals("Polo")).findFirst().isPresent());
        assertTrue(carList.stream().filter(o -> o.getModel().equals("X5")).findFirst().isPresent());
    }

    @Test
    public void findCarByBrand() {
        Car car = new Car("Abc123", "Alice", "BMW", "X1", new Date());
        carRepository.save(car);
        Car car2 = new Car("Abc124", "Bob", "BMW", "X5", new Date());
        carRepository.save(car2);

        List<Car> carList = carRepository.findByBrandIgnoreCaseOrderByNumber("bmw");

        assertTrue(carList.stream().filter(o -> o.getNumber().equals("Abc123")).findFirst().isPresent());
        assertTrue(carList.stream().filter(o -> o.getNumber().equals("Abc124")).findFirst().isPresent());

        assertTrue(carList.stream().filter(o -> o.getOwner().equals("Bob")).findFirst().isPresent());
        assertTrue(carList.stream().filter(o -> o.getOwner().equals("Alice")).findFirst().isPresent());

        assertTrue(carList.stream().filter(o -> o.getModel().equals("X1")).findFirst().isPresent());
        assertTrue(carList.stream().filter(o -> o.getModel().equals("X5")).findFirst().isPresent());
    }

    @Test
    public void updateCar() {
        Car car = new Car("Abc123", "Bob", "BMW", "X1", new Date());
        carRepository.save(car);
        car.setOwner("Alice");
        car.setBrand("Volkswagen");
        car.setModel("Polo");
        carRepository.save(car);
        Car carFound = carRepository.findByNumberIgnoreCase("abc123");
        assertEquals("Abc123", carFound.getNumber()); //Number should be unchanged.
        assertEquals("Alice", carFound.getOwner());
        assertEquals("Volkswagen", carFound.getBrand());
        assertEquals("Polo", carFound.getModel());

    }

    @Test
    public void deleteCar() {
        Car car = new Car("Abc123", "Bob", "BMW", "X1", new Date());
        carRepository.save(car);
        carRepository.delete("Abc123");
        Car carFound = carRepository.findByNumberIgnoreCase("Abc123");
        assertNull(carFound);
    }
}
