package org.liangfeng.carcat;

import java.util.Date;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import com.fasterxml.jackson.databind.ObjectMapper;


import org.liangfeng.carcat.repository.CarRepository;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;
import org.liangfeng.carcat.domain.Car;

/**
 * The application layer tests without starting server
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CarCatApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CarRepository carRepository;

    private Car mockCar = new Car("TEST999", "TESTER", "TEST_BRAND", "TEST_MODEL", new Date());

    @Before
    public void setup() throws Exception {
        carRepository.save(mockCar);
    }

    @After
    public void cleanup() throws Exception {
        carRepository.deleteAll();
    }

//    @Test
//    public void shouldReturnRepositoryIndex() throws Exception {
//        mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk()).andExpect(
//                jsonPath("$._links.carcat").exists());
//    }
    @Test
    public void listCarCatalog() throws Exception {
        mockMvc.perform(get("/carcat/list")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("TEST999")));
    }
    @Test
    public void addCarToCatalog() throws Exception {
        Car car = new Car("AAA111", "Alice", "Volkswagen", "POLO", new Date());
        mockMvc.perform(post("/carcat/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(car)))
                .andExpect(status().isCreated());
    }
    @Test
    public void addDuplicatedCarNumber() throws Exception {
        Car car = new Car(mockCar.getNumber().toLowerCase(), "Alice", "Volkswagen", "POLO", new Date());
        mockMvc.perform(post("/carcat/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(car)))
                .andExpect(status().isBadRequest()); //registration number Already exist in the DB
    }
    @Test
    public void searchByOwner() throws Exception {
        mockMvc.perform(get("/carcat/searchByOwner?owner=" + mockCar.getOwner())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(mockCar.getNumber())))
                .andExpect(content().string(containsString(mockCar.getBrand())))
                .andExpect(content().string(containsString(mockCar.getOwner())))
                .andExpect(content().string(containsString(mockCar.getModel())))
                .andExpect(content().string(containsString(mockCar.getBrand())));
    }

    @Test
    public void searchByBrand() throws Exception {
        mockMvc.perform(get("/carcat/searchByBrand?brand=" + mockCar.getBrand())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(mockCar.getNumber())))
                .andExpect(content().string(containsString(mockCar.getBrand())))
                .andExpect(content().string(containsString(mockCar.getOwner())))
                .andExpect(content().string(containsString(mockCar.getModel())))
                .andExpect(content().string(containsString(mockCar.getBrand())));

    }

    @Test
    public void showByNumber() throws Exception {
        mockMvc.perform(get("/carcat/item/" + mockCar.getNumber())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().string(containsString("TESTER")));
    }

    @Test
    public void deleteCar() throws Exception {
        mockMvc.perform(delete("/carcat/delete/" + mockCar.getNumber()))
                .andExpect(status().isNoContent());
        mockMvc.perform(get("/carcat/show/TEST999"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateCar() throws Exception {
        Car car = new Car(mockCar.getNumber(), "Alice", "Volkswagen", "POLO", new Date());
        mockMvc.perform(put("/carcat/edit/" + mockCar.getNumber())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(car)))
                .andExpect(status().isNoContent());
        mockMvc.perform(get("/carcat/item/" + mockCar.getNumber())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(car.getNumber())))
                .andExpect(content().string(containsString(car.getBrand())))
                .andExpect(content().string(containsString(car.getOwner())))
                .andExpect(content().string(containsString(car.getModel())))
                .andExpect(content().string(containsString(car.getBrand())));
    }
}