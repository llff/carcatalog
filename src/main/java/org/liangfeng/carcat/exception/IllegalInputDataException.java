package org.liangfeng.carcat.exception;

/**
 * For HTTP 400 Bad request exception
 */
public final class IllegalInputDataException extends RuntimeException {
    public IllegalInputDataException() {
        super();
    }

    public IllegalInputDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalInputDataException(String message) {
        super(message);
    }

    public IllegalInputDataException(Throwable cause) {
        super(cause);
    }
}