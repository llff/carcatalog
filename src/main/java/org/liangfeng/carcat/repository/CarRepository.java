package org.liangfeng.carcat.repository;

import java.util.List;

import org.liangfeng.carcat.domain.Car;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "cars", itemResourceRel = "car", path = "carcat")
public interface CarRepository extends PagingAndSortingRepository<Car, String> {

    Car findByNumberIgnoreCase(@Param("number") String number);

    List<Car> findByOwnerIgnoreCase(@Param("owner") String owner);

    List<Car> findByBrandIgnoreCaseOrderByNumber(@Param("brand") String brand);

    Long deleteByNumber(String number);

}