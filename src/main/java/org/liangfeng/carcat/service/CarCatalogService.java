package org.liangfeng.carcat.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import org.liangfeng.carcat.domain.Car;
import org.liangfeng.carcat.repository.CarRepository;



@Service
public class CarCatalogService {

    final static Logger logger = LoggerFactory.getLogger(CarCatalogService.class);

    @Autowired
    private CarRepository carRepository;

    public CarCatalogService() {
    }
    public Car addCar(Car car) {
        return carRepository.save(car);
    }

    public Car findCar(String number) {
        return carRepository.findByNumberIgnoreCase(number);
    }

    public void updateCar(Car car) {
        carRepository.save(car);
    }

    public void deleteCar(String number) {
        carRepository.deleteByNumber(number);
    }

    public Page<Car> findCars(Integer page, Integer size) {
        return carRepository.findAll(new PageRequest(page, size));
    }
    public List<Car> findCarsByOwner(String owner) {
        return carRepository.findByOwnerIgnoreCase(owner);
    }
    public List<Car> findCarsByBrand(String brand) {
        return carRepository.findByBrandIgnoreCaseOrderByNumber(brand);
    }
}
