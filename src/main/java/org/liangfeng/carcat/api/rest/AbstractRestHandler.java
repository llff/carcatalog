package org.liangfeng.carcat.api.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.liangfeng.carcat.domain.ErrorResponse;
import org.liangfeng.carcat.exception.IllegalInputDataException;
import org.liangfeng.carcat.exception.DataNotFoundException;


/**
 * This class is meant to be extended by all REST resource "controllers".
 * It contains exception mapping and other common REST API functionality.
 *
 * Currently only IllegalInputDataException and DataNotFoundException exceptions.
 */
public abstract class AbstractRestHandler implements ApplicationEventPublisherAware {

    final static Logger logger = LoggerFactory.getLogger(AbstractRestHandler.class);

    protected ApplicationEventPublisher eventPublisher;

    protected static final String  DEFAULT_PAGE_SIZE = "10";
    protected static final String DEFAULT_PAGE_NUM = "0";

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalInputDataException.class)
    public
    @ResponseBody
    ErrorResponse handleIllegalInputDataException(IllegalInputDataException ex, WebRequest request, HttpServletResponse response) {
        logger.error("Failed to store data due to invalid input in the request: ", ex.getMessage());
        return new ErrorResponse(ex, "Invalid input data in the request.");
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(DataNotFoundException.class)
    public
    @ResponseBody
    ErrorResponse handleResourceNotFoundException(DataNotFoundException ex, WebRequest request, HttpServletResponse response) {
        logger.error("Unable to find the resource: ", ex.getMessage());
        return new ErrorResponse(ex, "Resource was not found.");
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.eventPublisher = applicationEventPublisher;
    }
}
