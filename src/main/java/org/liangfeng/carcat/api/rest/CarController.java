package org.liangfeng.carcat.api.rest;


import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import org.liangfeng.carcat.domain.Car;
import org.liangfeng.carcat.exception.IllegalInputDataException;
import org.liangfeng.carcat.exception.DataNotFoundException;
import org.liangfeng.carcat.service.CarCatalogService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The Cat catalog API implementation
 * Basic add/delete/edit/search are implemented.
 *
 * The API document is auto-generated by SWAGGER2
 */
@RestController
@RequestMapping(value = "/carcat")
@Api(tags = {"Car Catalog API document"})
public class CarController extends AbstractRestHandler {

    private final static Logger logger = LoggerFactory.getLogger(CarController.class);

    @Autowired
    private CarCatalogService carCatalogService;

    @RequestMapping(value = "/add", method = RequestMethod.POST,
            consumes = {"application/json"},
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Add a car to catalog database.", notes = "Returns the URL of the new resource in the Location header.")
    public void addCar(@RequestBody Car car,
                       HttpServletRequest request, HttpServletResponse response) {
        if (car.getNumber() == null || car.getNumber().isEmpty()) {
            throw new IllegalInputDataException("The car registration number must not be provided!");
        }
        if (carCatalogService.findCar(car.getNumber()) != null) {
            throw new IllegalInputDataException("The car '"+car.getNumber()+"' already exist in the catalog.");
        }
        Car newCar = carCatalogService.addCar(car);
        response.setHeader("Location", request.getServletPath()+ "/item/"+ newCar.getNumber());
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "List all cars in the catalog database.", notes = "You can provide a page number (default "+DEFAULT_PAGE_NUM+") and a page size (default "+DEFAULT_PAGE_SIZE+")")
    public @ResponseBody Page<Car> findCars(
            @ApiParam(value = "The page number (zero-based)", required = true) @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) Integer page,
            @ApiParam(value = "Tha page size", required = true) @RequestParam(value = "size", required = true, defaultValue = DEFAULT_PAGE_SIZE) Integer size,
                      HttpServletRequest request, HttpServletResponse response)
    {
        return carCatalogService.findCars(page, size);
    }

    @RequestMapping(value = "/item/{num}",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Show a car in the catalog with registration number.", notes = "A valid car registration number (case sensitive) in the URL query parameter should be supplied")
    public @ResponseBody Car getCar(@ApiParam(value = "The registration number of the car.", required = true) @PathVariable("num") String num,
                                     HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Car car = this.carCatalogService.findCar(num);
        if (car == null) {
            throw new DataNotFoundException("Car with number '"+ num +"' was not found.");
        }
        return car;
    }

    @RequestMapping(value = "/searchByOwner",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Find all cars owned by a owner.", notes = "The owner (case sensitive) of the car in URL query parameter should be supplied.")
    public @ResponseBody List<Car> findCarsByOwner(@ApiParam(value = "The car owner.", required = true) @RequestParam("owner") String owner,
                                     HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        List<Car> cars = this.carCatalogService.findCarsByOwner(owner);
        if (cars == null) {
            throw new DataNotFoundException("Car owned by '"+ owner + "' was not found.");
        }
        return cars;
    }

    @RequestMapping(value = "/searchByBrand",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Find all cars within same brand.", notes = "The brand of the car(s) in URL query parameter should be supplied.")
    public @ResponseBody List<Car> findCarsByBrand(@ApiParam(value = "The brand of the car(s).", required = true) @RequestParam("brand") String brand,
                                                   HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        List<Car> cars = this.carCatalogService.findCarsByBrand(brand);
        if (cars == null) {
            throw new DataNotFoundException("Car with brand '"+ brand + "' was not found.");
        }
        return cars;
    }

    @RequestMapping(value = "/edit/{num}",
            method = RequestMethod.PUT,
            consumes = {"application/json"},
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Update a car's properties.", notes = "A valid car registration number in the URL and in the payload should be supplied and matched.")
    public void updateCar(@ApiParam(value = "The registration number of the car to be updated", required = true)
                            @PathVariable("num") String num, @RequestBody Car car,
                            HttpServletRequest request, HttpServletResponse response) {
        Car toUpdate = this.carCatalogService.findCar(num);
        if (toUpdate == null) {
            throw new DataNotFoundException("Update failed - car with number '"+ num +"' was not found.");
        }
        if (num.compareToIgnoreCase(car.getNumber()) != 0) { //the registration number cannot be updated
            throw new IllegalInputDataException("Car registration number in request body and URL are not matched.");
        }
        car.setNumber(toUpdate.getNumber()); //Use orignal number instead
        this.carCatalogService.updateCar(car);
    }

    @RequestMapping(value = "/delete/{num}",
            method = RequestMethod.DELETE,
            produces = {"application/json", "application/xml"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Remove a car entity from catalog database.", notes = "A valid car registration number in the URL should be supplied. The resource can NOT be recovered after delete!")
    public void deleteCar(@ApiParam(value = "The registration number of the car to be deleted.", required = true)
                            @PathVariable("num") String num, HttpServletRequest request,
                            HttpServletResponse response) {
        Car car = this.carCatalogService.findCar(num);
        if (car == null) {
            throw new DataNotFoundException("Delete failed - car with number '"+ num + "' was not found.");
        }
        this.carCatalogService.deleteCar(car.getNumber());
    }
}
