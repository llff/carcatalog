package org.liangfeng.carcat;


import javax.annotation.PostConstruct;
import org.liangfeng.carcat.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CarCatApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarCatApplication.class, args);
	}

	@Autowired
	private CarRepository repository;

	@PostConstruct
	public void postStartingup() {

	}
}
