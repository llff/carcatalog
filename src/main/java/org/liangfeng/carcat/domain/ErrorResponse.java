package org.liangfeng.carcat.domain;

public class ErrorResponse {
    public final String error;
    public final String details;

    public ErrorResponse(Exception ex, String error) {
        this.error = error;
        this.details = ex.getLocalizedMessage();
    }
}
