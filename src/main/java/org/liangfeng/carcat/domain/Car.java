package org.liangfeng.carcat.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.Id;

import java.util.Date;

public class Car {

    @Id //implicit unique
    private String number;

    private String owner;

    private String brand;

    private String model;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date manufactureDate;

    public Car(){
    }

    public Car(String number, String owner, String brand, String model, Date manfctDate){
        this.number = number;
        this.owner = owner;
        this.brand = brand;
        this.model = model;
        this.manufactureDate = manfctDate;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(Date date) {
        this.manufactureDate = date;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}